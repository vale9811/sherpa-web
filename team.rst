.. _SherpaTeam:

###########
Sherpa Team
###########

You can reach the Sherpa team with your questions, comments and praise
at
`​sherpa@projects.hepforge.org <mailto:sherpa@projects.hepforge.org>`__.

Bug reports should be entered into our `​issue
tracker <https://gitlab.com/sherpa-team/sherpa/issues>`__.

If you would like to receive announcements of new Sherpa releases and
important updates, you may subscribe to our `​announcement mailing
list <http://www.hepforge.org/lists/listinfo/sherpa-announce>`__.

To discuss Sherpa usage issues with other users, there is a public
mailing list at
`​http://www.hepforge.org/lists/listinfo/sherpa-discuss <http://www.hepforge.org/lists/listinfo/sherpa-discuss>`__.

.. _Currentmembers:

Current members
===============

.. _EnricoBothmann:

Enrico Bothmann
---------------

-  PostDoc at at `​Higgs Centre Edinburgh <https://higgs.ph.ed.ac.uk>`__
-  On-the-fly reweighting, CSShower, framework I/O

.. _StefanHoeche:

Stefan Hoeche
-------------

-  ME+PS merging, NLO, HERA physics,
   `​Comix <http://comix.freacafe.de>`__, Parton Showers
-  `​personal website <http://www.freacafe.de>`__

.. _FrankKrauss:

Frank Krauss
------------

-  Overall event generation framework, ME+PS merging, underlying event,
   hadronization models
-  `​personal website <http://www.ippp.dur.ac.uk/~krauss>`__

.. _SilvanKuttimalai:

Silvan Kuttimalai
-----------------

-  PostDoc at `​SLAC <http://www.slac.stanford.edu>`__
-  BSM, Comix

.. _MarekSchoenherr:

Marek Schoenherr
----------------

-  PostDoc at `​IPPP Durham <http://www.ippp.dur.ac.uk/>`__
-  ME+PS merging, NLO, Photon radiation, SUSY decays

.. _SteffenSchumann:

Steffen Schumann
----------------

-  Many body final states, BSM physics, ME+PS merging
-  `​personal
   website <http://www.thphys.uni-heidelberg.de/~schumann/>`__

.. _FrankSiegert:

Frank Siegert
-------------

-  ME+PS merging with tree-level and one-loop amplitudes
-  Decays in the hard scattering process and hadron/tau decays
-  ATLAS support
-  `​personal website <http://fsiegert.web.cern.ch/fsiegert/>`__

.. _JenniferThompson:

Jennifer Thompson
-----------------

-  PhD student at `​IPPP Durham <http://www.ippp.dur.ac.uk/>`__
-  NLO, ME+PS merging at NLO

.. _JanWinter:

Jan Winter
----------

-  PostDoc `​MPI Munich <https://www.mpp.mpg.de/english/>`__
-  Parton Showers, NLO, hadronization models

.. _KorinnaZapp:

Korinna Zapp
------------

-  PostDoc `​CERN <http://ph-dep-th.web.cern.ch/ph-dep-th/>`__
-  Minimum Bias, Parton Showers

.. _Formermembers:

Former members
==============

.. _JenniferArchibald:

Jennifer Archibald
------------------

-  Former PhD student (2007-2011)
-  QED subtraction, massive QCD subtraction, SUSY QCD subtraction
-  now at `​Carpmaels&Ransford <http://www.carpmaels.com>`__

.. _TimoFischer:

Timo Fischer
------------

-  Former diploma student (2005-2006)
-  Exotic physics models, helicity method for spin 3/2 particles
-  now at `​Uni
   Goettingen <http://www.theorie.physik.uni-goettingen.de/~fischer/>`__

.. _TanjuGleisberg:

Tanju Gleisberg
---------------

-  Former diploma and PhD student in Dresden (2002-2007), PostDoc at
   SLAC (2007-2010)
-  Exotic physics models, phase-space integration, Catani-Seymour
   subtraction
-  now at `​ISEG HV <http://www.iseg-hv.com/>`__

.. _HendrikHoeth:

Hendrik Hoeth
-------------

-  Former staff member (2008-2013)
-  Hadronization, Minimum Bias, Tuning

.. _RalfKuhn:

Ralf Kuhn
---------

-  Former diploma and PhD student in Dresden (1997-2002)
-  now at [`​ralf.kuhn@web.de <mailto:ralf.kuhn@web.de>`__ software
   company]

.. _ThomasLaubrich:

Thomas Laubrich
---------------

-  Former diploma student in Dresden (2005-2006)
-  Tau decays
-  now at `​d-fine <http://www.d-fine.de/en/>`__

.. _RadoslawMatyszkiewicz:

Radoslaw Matyszkiewicz
----------------------

-  Former postdoc in Dresden (2005-2007)
-  MHV formalism for matrix elements

.. _AndreasSchaelicke:

Andreas Schaelicke
------------------

-  Former diploma and PhD student in Dresden (1999-2005)
-  Parton showers, ME+PS merging
-  now at `​DESY Zeuthen <http://www-zeuthen.desy.de/~dreas/>`__
