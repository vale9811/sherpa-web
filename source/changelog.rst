.. _changelog:

######################
Releases and Changelog
######################

There is a release branch for every release which includes backports
of bugfixes but keeps the api stable. You may acquire it by the means
of:

- ``git clone -b rel-a-b-c https://gitlab.com/sherpa-team/sherpa.git``

where ``a``, ``b``, and ``c`` correspond to the version number
``a.b.c``.


The following is autogenerated from our `gitlab tags <https://gitlab.com/sherpa-team/sherpa/-/tags>`_.


.. gitlab-tags::
   :api-url: https://gitlab.com/api/v4/
   :project: 5781712
   :fulltext: 1
   :tag-link-text: Release notes on GitLab.
   :header-prefix: Sherpa
