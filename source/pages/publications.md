```label
publications
```
Publications
============

A practical guide to event generation for prompt photon production
------------------------------------------------------------------

-   Authors: Frank Siegert
-   The production of prompt photons is one of the most relevant
    scattering processes studied at hadron-hadron colliders in recent
    years. This article will give an overview of the different
    approaches used to simulate prompt photon production in the Sherpa
    event generator framework. Special emphasis is placed on a complete
    simulation of this process including fragmentation configurations.
    As a novel application a merged simulation of γγ and γγ+jet
    production at NLO accuracy is presented and compared to measurements
    from the ATLAS experiment.
-   [​arXiv:1611.07226](http://inspirehep.net/search?p=arXiv:1611.07226)

Next-to-leading order QCD predictions for top-quark pair production with up to three jets
-----------------------------------------------------------------------------------------

-   Authors: Stefan Höche, Philipp Maierhoefer, Niccolo Moretti, Stefano
    Pozzorini, Frank Siegert
-   We present theoretical predictions for the production of top-quark
    pairs with up to three jets at the next-to leading order in
    perturbative QCD. The relevant calculations are performed with
    Sherpa and OpenLoops?. To address the issue of scale choices and
    related uncertainties in the presence of multiple scales, we compare
    results obtained with the standard scale HT/2 at fixed order and the
    MINLO procedure. Analyzing various cross sections and distributions
    for tt+0,1,2,3 jets at the 13 TeV LHC we find a remarkable overall
    agreement between fixed-order and MINLO results. The differences are
    typically below the respective factor-two scale variations,
    suggesting that for all considered jet multiplicities, missing
    higher-order effects should not exceed the ten percent level.
-   [​arXiv:1607.06934](http://inspirehep.net/search?p=arXiv:1607.06934)

The midpoint between dipole and parton showers
----------------------------------------------

-   Authors: Stefan Hoeche, Stefan Prestel
-   We present a new parton-shower algorithm. Borrowing from the basic
    ideas of dipole cascades, the evolution variable is judiciously
    chosen as the transverse momentum in the soft limit. This leads to a
    very simple analytic structure of the evolution. A weighting
    algorithm is implemented, that allows to consistently treat
    potentially negative values of the splitting functions and the
    parton distributions. We provide two independent, publicly available
    implementations for the two event generators Pythia and Sherpa.
-   [​arXiv:1506.05057](http://inspirehep.net/search?p=1506.05057)

Beyond Standard Model calculations with Sherpa
----------------------------------------------

-   Authors: Stefan Hoeche, Silvan Kuttimalai, Steffen Schumann, Frank
    Siegert
-   We present a fully automated framework as part of the Sherpa event
    generator for the computation of tree-level cross sections in beyond
    Standard Model scenarios, making use of model information given in
    the Universal FeynRules? Output format. Elementary vertices are
    implemented into C++ code automatically and provided to the
    matrix-element generator Comix at runtime. Widths and branching
    ratios for unstable particles are computed from the same building
    blocks. The corresponding decays are simulated with spin
    correlations. Parton showers, QED radiation and hadronization are
    added by Sherpa, providing a full simulation of arbitrary BSM
    processes at the hadron level.
-   [​arXiv:1412.6478](http://inspirehep.net/search?p=1412.6478)

Soft evolution of multi-jet final states
----------------------------------------

-   Authors: Erik Gerwick, Stefan Hoeche, Simone Marzani, Steffen
    Schumann
-   We present a new framework for computing resummed and matched
    distributions in processes with many hard QCD jets. The intricate
    color structure of soft gluon emission at large angles renders
    resummed calculations highly non-trivial in this case. We automate
    all ingredients necessary for the color evolution of the soft
    function at next-to-leading-logarithmic accuracy, namely the
    selection of the color bases and the projections of color operators
    and Born amplitudes onto those bases. Explicit results for all QCD
    processes with up to 2→5 partons are given. We also devise a new
    tree-level matching scheme for resummed calculations which exploits
    a quasi-local subtraction based on the Catani-Seymour dipole
    formalism. We implement both resummation and matching in the Sherpa
    event generator. As a proof of concept, we compute the resummed and
    matched transverse-thrust distribution for hadronic collisions.
-   [​arXiv:1411.7325](http://inspirehep.net/search?p=1411.7325)

Higgs-boson production through gluon fusion at NNLO QCD with parton showers
---------------------------------------------------------------------------

-   Authors: Stefan Hoeche, Ye Li, Stefan Prestel
-   We discuss how the UN2LOPS scheme for matching NNLO calculations to
    parton showers can be applied to processes with large higher-order
    perturbative QCD corrections. We focus on Higgs-boson production
    through gluon fusion as an example. We also present an NNLO
    fixed-order event generator for this reaction.
-   [​arXiv:1407.3773](http://inspirehep.net/search?p=1407.3773)

Drell-Yan lepton pair production at NNLO QCD with parton showers
----------------------------------------------------------------

-   Authors: Stefan Hoeche, Ye Li, Stefan Prestel
-   We present a simple approach to combine NNLO QCD calculations and
    parton showers, based on the UNLOPS technique. We apply the method
    to the computation of Drell-Yan lepton-pair production at the Large
    Hadron Collider. We comment on possible improvements and intrinsic
    uncertainties.
-   [​arXiv:1405.3607](http://inspirehep.net/search?p=1405.3607)

Triple vector boson production through Higgs-Strahlung with NLO multijet merging
--------------------------------------------------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Stefano Pozzorini, Marek
    Schoenherr, Jennifer M. Thompson, Korinna C. Zapp
-   Triple gauge boson hadroproduction, in particular the production of
    three W-bosons at the LHC, is considered at next-to leading order
    accuracy in QCD. The NLO matrix elements are combined with parton
    showers. Multijet merging is invoked such that NLO matrix elements
    with one additional jet are also included. The studies here
    incorporate both the signal and all relevant backgrounds for VH
    production with the subsequent decay of the Higgs boson into W- or
    τ-pairs. They have been performed using Sherpa+OpenLoops? in
    combination with Collier.
-   [​arXiv:1403.7516](http://inspirehep.net/search?p=1403.7516)

Next-to-leading order QCD predictions for top-quark pair production with up to two jets merged with a parton shower
-------------------------------------------------------------------------------------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Philipp Maierhoefer, Stefano
    Pozzorini, Marek Schonherr, Frank Siegert
-   We present differential cross sections for the production of
    top-quark pairs in conjunction with up to two jets, computed at
    next-to leading order in perturbative QCD and consistently merged
    with a parton shower in the Sherpa+OpenLoops? framework. Top quark
    decays including spin correlation effects are taken into account at
    leading order accuracy. The calculation yields a unified description
    of top-pair plus multi-jet production, and detailed results are
    presented for various key observables at the Large Hadron Collider.
    A large improvement is found for the total transverse energy
    spectrum, which plays a prominent role in searches for physics
    beyond the Standard Model.
-   [​arXiv:1402.6293](http://inspirehep.net/search?p=1402.6293)

Uncertainties in <MEPS@NLO> calculations of h+jets
--------------------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Marek Schoenherr
-   Uncertainties in the simulation of Higgs boson production with up to
    two jets at next-to leading order accuracy are investigated.
    Traditional uncertainty estimates based on scale variations are
    extended employing different functional forms for the central scale,
    and the impact of details in the implementation of the parton shower
    is discussed.
-   [​arXiv:1401.7971](http://inspirehep.net/search?p=1401.7971)

NLO matching for ttbb production with massive b-quarks
------------------------------------------------------

-   Authors: Fabio Cascioli, Philipp Maierhoefer, Niccolo Moretti,
    Stefano Pozzorini, Frank Siegert
-   Theoretical uncertainties in the simulation of ttbb production
    represent one of the main obstacles that still hamper the
    observation of Higgs-boson production in association with top-quark
    pairs in the H-\>bb channel. In this letter we present a
    next-to-leading order (NLO) simulation of ttbb production with
    massive b-quarks matched to the Sherpa parton shower. This allows
    one to extend NLO predictions to arbitrary ttbb kinematics,
    including the case where one or both b-jets arise from collinear
    g-\>bb splittings. We find that this splitting mechanism plays an
    important role for the ttH(bb) analysis.
-   [​arXiv:1309.5912](http://inspirehep.net/search?p=arXiv:1309.5912)

Precise Higgs-background predictions: merging NLO QCD and squared quark-loop corrections to four-lepton + 0,1 jet production
----------------------------------------------------------------------------------------------------------------------------

-   Authors: Fabio Cascioli, Stefan Hoeche, Frank Krauss, Philipp
    Maierhofer, Stefano Pozzorini, Frank Siegert
-   We present precise predictions for four-lepton plus jets production
    at the LHC obtained within the fully automated Sherpa+OpenLoops?
    framework. Off-shell intermediate vector bosons and related
    interferences are consistently included using the complex-mass
    scheme. Four-lepton plus 0- and 1-jet final states are described at
    NLO accuracy, and the precision of the simulation is further
    increased by squared quark-loop NNLO contributions in the gg -\> 4l,
    gg -\> 4l+g, gq -\> 4l+q, and qq -\> 4l+g channels. These NLO and
    NNLO contributions are matched to the Sherpa parton shower, and the
    0- and 1-jet final states are consistently merged using the
    <MEPS@NLO> technique. Thanks to Sudakov resummation, the parton
    shower provides improved predictions and uncertainty estimates for
    exclusive observables. This is important when jet vetoes or jet bins
    are used to separate four-lepton final states arising from Higgs
    decays, diboson production, and top-pair production. Detailed
    predictions are presented for the ATLAS and CMS H-\>WW analyses at 8
    TeV in the 0- and 1-jet bins. Assessing renormalisation-,
    factorisation- and resummation-scale uncertainties, which reflect
    also unknown subleading Sudakov logarithms in jet bins, we find that
    residual perturbative uncertainties are as small as a few percent.
-   [​arXiv:1309.0500](http://inspirehep.net/search?p=arXiv:1309.0500)

Zero and one jet combined NLO analysis of the top quark forward-backward asymmetry
----------------------------------------------------------------------------------

-   Authors: Stefan Hoeche, Junwu Huang, Gionata Luisoni, Marek
    Schoenherr, Jan Winter
-   We present an analysis of the forward-backward asymmetry in the
    production of top quark pairs at the Tevatron collider. We use novel
    Monte Carlo methods for merging matrix elements and parton showers
    to combine NLO QCD predictions for tt and tt+jet production.
    Theoretical uncertainties are quantified in detail. We find
    agreement with experimental data on the transverse momentum
    dependence of the asymmetry.
-   [​Phys.Rev.D88 (2013)
    014040](http://inspirehep.net/search?p=1306.2703)

Uncertainties in NLO + parton shower matched simulations of inclusive jet and dijet production
----------------------------------------------------------------------------------------------

-   Authors: Stefan Hoeche, Marek Schonherr
-   We quantify uncertainties in the Monte-Carlo simulation of inclusive
    and dijet final states, which arise from using the <MC@NLO>
    technique for matching next-to-leading order parton level
    calculations and parton showers. We analyse a large variety of data
    from early measurements at the LHC. In regions of phase space where
    Sudakov logarithms dominate over high-energy effects, we observe
    that the main uncertainty can be ascribed to the free parameters of
    the parton shower. In complementary regions, the main uncertainty
    stems from the considerable freedom in the simulation of underlying
    events.
-   [​Phys.Rev.D86 (2012)
    094042](http://inspirehep.net/search?p=1208.2815)

NLO QCD matrix elements + parton showers in e+e- -\> hadrons
------------------------------------------------------------

-   Authors: Thomas Gehrmann, Stefan Hoeche, Frank Krauss, Marek
    Schonherr, Frank Siegert
-   We present a new approach to combine multiple NLO parton-level
    calculations matched to parton showers into a single inclusive event
    sample. The method provides a description of hard multi-jet
    configurations at next-to leading order in the perturbative
    expansion of QCD, and it is supplemented with the all-orders
    resummed modelling of jet fragmentation provided by the parton
    shower. The formal accuracy of this technique is discussed in
    detail, invoking the example of electron-positron annihilation into
    hadrons. We focus on the effect of renormalisation scale variations
    in particular. Comparison with experimental data from LEP underlines
    that this novel formalism describes data with a theoretical accuracy
    that has hitherto not been achieved in standard Monte Carlo event
    generators.
-   [​JHEP01 (2013) 144](http://inspirehep.net/search?p=1207.5031)

QCD matrix elements + parton showers: The NLO case
--------------------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Marek Schonherr, Frank Siegert
-   We present a process-independent technique to consistently combine
    next-to-leading order parton-level calculations of varying jet
    multiplicity and parton showers. Double counting is avoided by means
    of a modified truncated shower scheme. This method preserves both
    the fixed-order accuracy of the parton-level result and the
    logarithmic accuracy of the parton shower. We discuss the
    renormalisation and factorisation scale dependence of the approach
    and present results from an automated implementation in the Sherpa
    event generator using the test case of W-boson production at the
    Large Hadron Collider. We observe a dramatic reduction of
    theoretical uncertainties compared to existing methods which
    underlines the predictive power of our novel technique.
-   [​JHEP04 (2013) 027](http://inspirehep.net/search?p=1207.5030)

W+n-jet predictions with <MC@NLO> in Sherpa
-------------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Marek Schonherr, Frank Siegert
-   Results for the production of W-bosons in conjunction with up to
    three jets including parton shower corrections are presented and
    compared to recent LHC data. These results consistently incorporate
    the full next-to-leading order QCD corrections through the <MC@NLO>
    method, as implemented in the Sherpa event generator, with the
    virtual corrections obtained from the BlackHat? library.
-   [​Phys.Rev.Lett. 110 (2013)
    052001](http://inspirehep.net/search?p=1201.5882)

A critical appraisal of NLO+PS matching methods
-----------------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Marek Schonherr, Frank Siegert
-   In this publication, uncertainties in and differences between the
    <MC@NLO> and POWHEG methods for matching next-to-leading order QCD
    calculations with parton showers are discussed. Implementations of
    both algorithms within the event generator Sherpa are employed to
    assess the impact on a representative selection of observables. In
    the <MC@NLO> approach a phase space restriction has been added to
    subtraction and parton shower, which allows to vary in a transparent
    way the amount of non-singular radiative corrections that are
    exponentiated. Effects on various observables are investigated,
    using the production of a Higgs boson in gluon fusion, with or
    without an associated jet, as a benchmark process. The case of H+jet
    production is presented for the first time in an NLO+PS matched
    simulation. Uncertainties due to scale choices and non-perturbative
    effects are explored in the production of W and Z bosons in
    association with a jet. Corresponding results are compared to data
    from the Tevatron and LHC experiments.
-   [​JHEP09 (2012) 049](http://inspirehep.net/record/944643)

NLO matrix elements and truncated showers
-----------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Marek Schoenherr, Frank
    Siegert
-   In this publication, an algorithm is presented that combines the
    ME+PS approach to merge sequences of tree-level matrix elements into
    inclusive event samples with the POWHEG method, which combines exact
    next-to-leading order matrix element results with the parton shower.
    It was developed in parallel to the MENLOPS technique and has been
    implemented in the event generator Sherpa. The benefits of this
    approach are exemplified by some first predictions for a number of
    processes, namely the production of jets in e+ e- annihilation, in
    deep-inelastic ep scattering, in association with single W, Z or
    Higgs bosons, and with vector boson pairs at hadron colliders.
-   [​JHEP08 (2011)
    123](http://www-spires.dur.ac.uk/spires/find/hep/www?eprint=arXiv:1009.1127)

Automating the POWHEG method in Sherpa
--------------------------------------

-   Authors: Stefan Hoeche, Frank Krauss, Marek Schoenherr, Frank
    Siegert
-   A new implementation of the POWHEG method into the Monte-Carlo event
    generator Sherpa is presented, focusing on processes with a simple
    colour structure. Results for a variety of reactions, namely e+e- to
    hadrons, deep-inelastic lepton-nucleon scattering, hadroproduction
    of single vector bosons and of vector boson pairs as well as the
    production of Higgs bosons in gluon fusion serve as test cases for
    the successful realisation. The algorithm is fully automated such
    that for further processes only virtual matrix elements need to be
    included.
-   [​JHEP04 (2011)
    024](http://www-spires.dur.ac.uk/spires/find/hep/www?eprint=arXiv:1008.5399)

Hadronic final states in deep-inelastic scattering with Sherpa
--------------------------------------------------------------

-   Authors: Tancredi Carli, Thomas Gehrmann, Stefan Hoeche
-   We extend the multi-purpose Monte-Carlo event generator Sherpa to
    include processes in deeply inelastic lepton-nucleon scattering.
    Hadronic final states in this kinematical setting are characterised
    by the presence of multiple kinematical scales, which were up to now
    accounted for only by specific resummations in individual
    kinematical regions. Using an extension of the recently introduced
    method for merging truncated parton showers with higher-order
    tree-level matrix elements, it is possible to obtain predictions
    which are reliable in all kinematical limits. Different hadronic
    final states, defined by jets or individual hadrons, in
    deep-inelastic scattering are analysed and the corresponding results
    are compared to HERA data. The various sources of theoretical
    uncertainties of the approach are discussed and quantified. The
    extension to deeply inelastic processes provides the opportunity to
    validate the merging of matrix elements and parton showers in
    multi-scale kinematics inaccessible in other collider environments.
    It also allows to use HERA data on hadronic final states in the
    tuning of hadronisation models.
-   [​Eur.Phys.J.C67 (2010)
    73](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0912.3715)

Hard photon production and matrix-element parton-shower merging
---------------------------------------------------------------

-   Authors: Stefan Hoeche, Steffen Schumann, Frank Siegert
-   We present a Monte-Carlo approach to prompt-photon production, where
    photons and QCD partons are treated democratically. The photon
    fragmentation function is modelled by an interleaved QCD+QED parton
    shower. This known technique is improved by including higher-order
    real-emission matrix elements. To this end, we extend a recently
    proposed algorithm for merging matrix elements and truncated parton
    showers. We exemplify the quality of the Monte-Carlo predictions by
    comparing them to measurements of the photon fragmentation function
    at LEP and to measurements of prompt photon and diphoton production
    from the Tevatron experiments.
-   [​Phys.Rev.D81 (2010)
    034026](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0912.3501)

QCD matrix elements and truncated showers
-----------------------------------------

-   Authors: S. Hoeche, F. Krauss, S. Schumann, F. Siegert
-   We derive an improved prescription for the merging of matrix
    elements with parton showers, extending the CKKW approach. A
    flavour-dependent phase space separation criterion is proposed. We
    show that this new method preserves the logarithmic accuracy of the
    shower, and that the original proposal can be derived from it. One
    of the main requirements for the method is a truncated shower
    algorithm. We outline the corresponding Monte Carlo procedures and
    apply the new prescription to QCD jet production in e+e- collisions
    and Drell-Yan lepton pair production. Explicit colour information
    from matrix elements obtained through colour sampling is
    incorporated in the merging and the influence of different
    prescriptions to assign colours in the large N\_C limit is studied.
    We assess the systematic uncertainties of the new method.
-   [​JHEP05 (2009)
    053](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0903.1219)

Event generation with SHERPA 1.1.
---------------------------------

-   Authors: T. Gleisberg, S. Hoeche, F. Krauss, M. Schoenherr, S.
    Schumann, F. Siegert, J. Winter
-   In this paper the current release of the Monte Carlo event generator
    Sherpa, version 1.1, is presented. Sherpa is a general-purpose tool
    for the simulation of particle collisions at high-energy colliders.
    It contains a very flexible tree-level matrix-element generator for
    the calculation of hard scattering processes within the Standard
    Model and various new physics models. The emission of additional QCD
    partons off the initial and final states is described through a
    parton-shower model. To consistently combine multi-parton matrix
    elements with the QCD parton cascades the approach of Catani,
    Krauss, Kuhn and Webber is employed. A simple model of multiple
    interactions is used to account for underlying events in
    hadron\--hadron collisions. The fragmentation of partons into
    primary hadrons is described using a phenomenological
    cluster-hadronisation model. A comprehensive library for simulating
    tau-lepton and hadron decays is provided. Where available
    form-factor models and matrix elements are used, allowing for the
    inclusion of spin correlations; effects of virtual and real QED
    corrections are included using the approach of Yennie, Frautschi and
    Suura.
-   This is meant to be the standard reference if you are using versions
    of Sherpa \> 1.1.
-   [​JHEP02 (2009)
    007](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0811.4622)

Soft Photon Radiation in Particle Decays in SHERPA
--------------------------------------------------

-   Authors: Marek Schoenherr, Frank Krauss
-   In this paper the Yennie-Frautschi-Suura approach is used to
    simulate real and virtual QED corrections in particle decays. It
    makes use of the universal structure of soft photon corrections to
    resum the leading logarithmic QED corrections to all orders, and it
    allows a systematic correction of this approximate result to exact
    fixed order results from perturbation theory. The approach has been
    implemented as a Monte Carlo algorithm, which a posteriori modifies
    decay matrix elements through the emission of varying numbers of
    photons. The corresponding computer code is incorporated into the
    SHERPA event generator framework.
-   [​JHEP12 (2008)
    018](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0810.5071)

Comix, a new matrix element generator
-------------------------------------

-   Authors: T. Gleisberg, S. Hoeche
-   We present a new tree-level matrix element generator, based on the
    colour dressed Berends-Giele recursive relations. We discuss two new
    algorithms for phase space integration, dedicated to be used with
    large multiplicities and colour sampling.
-   [​JHEP12 (2008)
    039](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0808.3674)

How to calculate colourful cross sections efficiently
-----------------------------------------------------

-   Authors: T. Gleisberg, S. Hoeche, F. Krauss, R. Matyskiewicz
-   Different methods for the calculation of cross sections with many
    QCD particles are compared. To this end, CSW vertex rules,
    Berends-Giele recursion and Feynman-diagram based techniques are
    implemented as well as various methods for the treatment of colours
    and phase space integration. We find that typically there is only a
    small window of jet multiplicities, where the CSW technique has
    efficiencies comparable or better than both of the other two
    methods.
-   [​arXiv:0808.3672
    \[hep-ph](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0808.3672)
    \]

Comparative study of various algorithms for the merging of parton showers and matrix elements in hadronic collisions
--------------------------------------------------------------------------------------------------------------------

-   Authors: J. Alwall, S. Hoeche, F. Krauss, N. Lavesson, L. Lonnblad,

    F. Maltoni, M.L. Mangano, M. Moretti, C.G. Papadopoulos, F.
    Piccinini, S. Schumann, M. Treccani, J. Winter, M. Worek

-   We compare different procedures for combining fixed-order tree-level
    matrix-element generators with parton showers. We use the case of
    W-production at the Tevatron and the LHC to compare different
    implementations of the so-called CKKW and MLM schemes using
    different matrix-element generators and different parton cascades.
    We find that although similar results are obtained in all cases,
    there are important differences.

-   the long and published version of the extensive generator comparison

-   [​Eur.Phys.J.C53 (2008)
    473-500](http://www.slac.stanford.edu/spires/find/hep/www?eprint+0706.2569)

Matching parton showers and matrix elements
-------------------------------------------

-   Authors: S. Hoeche, F. Krauss, N. Lavesson, L. Lonnblad, M. Mangano,
    A.  Schaelicke, S. Schumann
-   We compare different procedures for combining fixed-order tree-level
    matrix element generators with parton showers. We use the case of
    W-production at the Tevatron and the LHC to compare different
    implementations of the so-called CKKW scheme and one based on the
    so-called MLM scheme using different matrix element generators and
    different parton cascades. We find that although similar results are
    obtained in all cases, there are important differences.
-   [​hep-ph/0602031](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=f+eprint+hep-ph%2F0602031)

Supersymmetry simulations with off-shell effects for LHC and ILC
----------------------------------------------------------------

-   Authors: K. Hagiwara, W. Kilian, F. Krauss, T. Ohl, T. Plehn, D.
    Rainwater, J. Reuter, S. Schumann
-   At the LHC and at an ILC, serious studies of new physics benefit
    from a proper simulation of signals and backgrounds. Using
    supersymmetric sbottom pair production as an example, we show how
    multi-particle final states are necessary to properly describe
    off-shell effects induced by QCD, photon radiation, or by
    intermediate on-shell states. To ensure the correctness of our
    findings we compare in detail the implementation of the
    supersymmetric Lagrangian in MadGraph?, Sherpa and Whizard. As a
    future reference we give the numerical results for several hundred
    cross sections for the production of supersymmetric particles,
    checked with all three codes.
-   [​Phys.Rev.D73 (2005)
    055005](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=f+eprint+hep-ph%2F0512260)

Studying W+W- production at the Fermilab Tevatron with SHERPA
-------------------------------------------------------------

-   Authors: T. Gleisberg, F. Krauss, A. Schaelicke, S. Schumann, J.C.
    Winter
-   The merging procedure of tree-level matrix elements with the
    subsequent parton shower as implemented in SHERPA will be studied
    for the example of W boson pair production at the Fermilab Tevatron.
    Comparisons with fixed order calculations at leading and
    next-to-leading order in the strong coupling constant and with other
    Monte Carlo simulations validate once more the impact and the
    quality of the merging algorithm and its implementation.
-   [​Phys.Rev.D72 (2005)
    034028](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0504032)

Implementing the ME+PS merging Algorithm.
-----------------------------------------

-   Authors: A. Schaelicke, F. Krauss
-   The method to merge matrix elements for multi particle production
    and parton showers in electron-positron annihilations and hadronic
    collisions and its implementation into the new event generator
    SHERPA is described in detail. Examples highlighting different
    aspects of it are thoroughly discussed, some results for various
    cases are presented. In addition, a way to extend this method to
    general electroweak interactions is presented.
-   [​JHEP 0507 (2005)
    018](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0503281)

Simulating W/Z + jets production at the CERN LHC.
-------------------------------------------------

-   Authors: F. Krauss, A. Schaelicke, S. Schumann, G.Soff
-   The merging procedure of tree-level matrix elements and the
    subsequent parton shower as implemented in the new event generator
    SHERPA will be validated for the example of single gauge boson
    production at the LHC. The validation includes consistency checks
    and comparisons to results obtained from other event generators. In
    particular, comparisons with full next-to-leading order QCD
    calculations prove SHERPA\'s ability to correctly account for
    additional hard QCD radiation present in these processes.
-   [​Phys.Rev.D72 (2005)
    054017](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0503280)

APACIC++ 2.0: A PArton Cascade In C++.
--------------------------------------

-   Authors: A. Schaelicke, F. Krauss, G. Soff
-   The new version of the parton shower module APACIC++ for the SHERPA
    event generator framework is presented. It incorporates some
    features, that are specific for the consistent merging with
    multi-particle matrix elements at tree-level. This publication also
    includes some exemplary results and a short description of the
    upgraded class structure of APACIC++, version 2.0.
-   [​Comput.Phys.Commun.174 (2006)
    876](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0503087)

Simulating W/Z + jets production at the Tevatron.
-------------------------------------------------

-   Authors: F. Krauss, A. Schaelicke, S. Schumann, G.Soff
-   The merging procedure of tree-level matrix elements and the
    subsequent parton shower as implemented in the new event generator
    SHERPA will be validated for the example of W/Z+jets production at
    the Tevatron. Comparisons with results obtained from other
    approaches and programs and with experimental results clearly show
    that the merging procedure yields relevant and correct results at
    both the hadron and parton levels.
-   [​Phys.Rev.D70 (2004)
    114009](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0409122)

Cross sections for multi-particle final states at a linear collider.
--------------------------------------------------------------------

-   Authors: T. Gleisberg, F. Krauss, C.G. Papadopoulos, A. Schaelicke,
    S.  Schumann
-   In this paper total cross sections for signals and backgrounds of
    top- and Higgs-production channels in electron-positron collisions
    at a future linear collider are presented. All channels considered
    are characterized by the emergence of six-particle final states. The
    calculation takes into account the full set of tree-level amplitudes
    in each process. Two multi-purpose parton level generators,
    HELAC/PHEGAS and AMEGIC++ are used, their results are found to be in
    perfect agreement.
-   [​Eur.Phys.J.C34 (2004)
    173-180](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0311273)

SHERPA 1.alpha, a proof-of-concept version.
-------------------------------------------

-   Authors: T. Gleisberg, S. Hoeche, F. Krauss, A. Schaelicke, S.
    Schumann, J. Winter
-   The new multipurpose event-generation framework SHERPA, acronym for
    Simulation for High-Energy Reactions of PArticles, is presented. It
    is entirely written in the object-oriented programming language C++.
    In its current form, it is able to completely simulate
    electron\--positron and unresolved photon\--photon collisions at
    high energies. Also, fully hadronic collisions, such as, e.g.,
    proton\--anti-proton, proton\--proton, or resolved photon\--photon
    reactions, can be described on the signal level.
-   [​JHEP02 (2004)
    056](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0311263)

A modified cluster hadronization model.
---------------------------------------

-   Authors: J. Winter, F. Krauss, G. Soff
-   A new phenomenological cluster-hadronization model is presented. Its
    specific features are the incorporation of soft colour reconnection,
    a more general treatment of diquarks including their spin and giving
    rise to clusters with baryonic quantum numbers, and a dynamic
    separation of the regimes of clusters and hadrons according to their
    masses and flavours. The distinction between the two regions
    automatically leads to different cluster decay and transformation
    modes. Additionally, these aspects require an extension of
    individual cluster-decay channels that were available in previous
    versions of such models.
-   [​Eur.Phys.J.C36 (2004)
    381-395](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0311085)

Helicity formalism for spin-2 particles
---------------------------------------

-   Authors: T. Gleisberg, F. Krauss, K. Matchev, A. Schaelicke, S.
    Schumann, G. Soff
-   We develop the helicity formalism for spin-2 particles and apply it
    to the case of gravity in flat extra dimensions. We then implement
    the large extra dimensions scenario of Arkani-Hamed, Dimopoulos and
    Dvali in the program AMEGIC++, allowing for an easy calculation of
    arbitrary processes involving the emission or exchange of gravitons.
    We complete the set of Feynman rules derived by Han, Lykken and
    Zhang, and perform several consistency checks of our implementation.
-   [​JHEP09 (2003)
    001](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0306182)

Matrix elements and parton showers in hadronic interactions
-----------------------------------------------------------

-   Authors: F. Krauss
-   A method is suggested to combine tree level QCD matrix for the
    production of multi jet final states and the parton shower in
    hadronic interactions. The method follows closely an algorithm
    developed recently for the case of `$e^+e^-$` annihilations.
-   [​JHEP08 (2002)
    015](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0205283)

Implementing initial state radiation for lepton induced processes in Amegic++
-----------------------------------------------------------------------------

-   Authors: A. Schalicke, F. Krauss, R. Kuhn, G. Soff
-   We have implemented the method of Yennie, Frautschi, and Suura up to
    first order in alpha for the simulation of QED Initial State
    Radiation in lepton induced processes in AMEGIC++. We consider
    s-channel processes via the exchange of scalar or vector resonances
    at electron and muon colliders.
-   [​JHEP12 (2002)
    013](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0203259)

AMEGIC++ 1.0: A Matrix Element Generator In C++
-----------------------------------------------

-   Authors: F. Krauss, R. Kuhn, G. Soff
-   The new matrix element generator AMEGIC++ is introduced, dedicated
    to describe multi-particle production in high-energy particle
    collisions. It automatically generates helicity amplitudes for the
    processes under consideration and constructs suitable, efficient
    integration channels for the multi-channel phase space integration.
    The corresponding expressions for the amplitudes and the integrators
    are stored in library files to be linked to the main program.
-   [​JHEP02 (2002)
    044](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0109036)

QCD Matrix elements + parton showers
------------------------------------

-   Authors: S. Catani, F. Krauss, R. Kuhn, B.R. Webber
-   We propose a method for combining QCD matrix elements and parton
    showers in Monte Carlo simulations of hadronic final states in
    `$e^+e^-$` annihilation. The matrix element and parton shower
    domains are separated at some value `$[y](){ini}$` of the jet
    resolution, defined according to the `$k_T$`-clustering algorithm.
    The matrix elements are modified by Sudakov form factors and the
    parton showers are subjected to a veto procedure to cancel
    dependence on `$[y](){ini}$` to next-to-leading logarithmic
    accuracy. The method provides a leading-order description of hard
    multi-jet configurations together with jet fragmentation, while
    avoiding the most serious problems of double counting. We present
    first results of an approximate implementation using the
    eventgenerator APACIC++.
-   [​JHEP11 (2001)
    063](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0109231)

APACIC++: A PArton Cascade In C++, version 1.0
----------------------------------------------

-   Authors: R. Kuhn, F. Krauss, B. Ivanyi, G. Soff
-   APACIC++ is a Monte-Carlo event-generator dedicated for the
    simulation of electron-positron annihilations into jets. Within the
    framework of APACIC++, the emergence of jets is identified with the
    perturbative production of partons as governed by corresponding
    matrix elements. In addition to the build-in matrix elements
    describing the production of two and three jets, further programs
    can be linked allowing for the simultaneous treatment of higher
    numbers of jets. APACIC++ hosts a new approach for the combination
    of arbitrary matrix elements for the production of jets with the
    parton shower, which in turn models the evolution of these jets. For
    the evolution, different ordering schemes are available, namely
    ordering by virtualities or by angles. At the present state, the
    subsequent hadronization of the partons is accomplished by means of
    the Lund-string model as provided within Pythia. An appropriate
    interface is provieded. The program takes full advantage of the
    object-oriented features provided by C++ allowing for an equally
    abstract and transparent programming style.
-   [​Comput. Phys. Commun.134 (2001)
    223](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+EPRINT+HEP-PH%2F0004270)
