```label
monte-carlo-info
```

Monte Carlo event generators
============================

Precision tests of the Standard Model and the quest for new physics in
many cases relies on the confrontation of theoretical calculations with
experimental findings. But such a procedure is far from being trivial
because

-   many signals under investigation rely on non-trivial final states
    involving many particles;
-   experimentally, these final states have to fulfill some (detector)
    cuts, leading to considerable difficulties when integrating over the
    appropriate phase space;
-   the objects in the calculations are often quarks and gluons, whereas
    experimentally only hadrons, i.e. their bound states, are detected,
    which necessitates to model the quantitatively poorly understood
    (phase-)transition from the perturbative to the non-perturbative
    regime of QCD, the theory of strong interactions;
-   apart from the already mentioned hadronization also detector effects
    wash out signals.

One of the most popular options to treat the theoretical difficulties
sketched above lies in the usage of event generators. They are based on
a kind of \"divide et impera\" (divide and rule) strategy. The key idea
is to separate events - as they could be seen through the detector -
into different stages according to some characteristic energy scale.
Starting from the highest scales, i.e., the shortest distances,
subprocesses are added which will populate different phases of particle
emission and creation at lower scales or larger distances. Schematically
this translates into

1.  considering the production of heavy and/or highly energetic
    particles through appropriate matrix elements that are exact at some
    perturbative order and respect quantum interferences etc.,
2.  radiating lighter particles - like for instance gluons and photons -
    which are softer and tend to be collinear with their emitters
    through the parton shower, which correctly resums the leading terms
    to all orders in perturbation theory,
3.  the eventual decay of heavy unstable particles, again through matrix
    elements, again dressed by parton showers to fill the phase space of
    multiple soft emission in the perturbative regime,
4.  the hadronization of the quarks and gluons into hadrons, which in
    many cases may not be stable,
5.  their decays into long-lived, i.e. practically stable, hadrons that
    finally hit the detector.

For a pictorial representation, check this out:
[![](http://sherpa.hepforge.org/event.png "http://sherpa.hepforge.org/event.png")](http://sherpa.hepforge.org/event.png)

However appealing, this nice picture is somewhat spoiled by various
additional problems, for instance:

-   more than often divergencies appear in (loop-level) calculations
    that cancel analytically when considering full perturbation theory
    but are extremely difficult to handle numerically;
-   especially for strong interacting initial states effects like
    multiple interactions of beam remnants or further beam particles,
    rescattering and generally the pile-up of these effects contribute
    significantly to the final state multiplicity and are thus
    important, their qualitative and quantitative understanding,
    however, is quite limited.

These problems are - to some extent - currently investigated.
