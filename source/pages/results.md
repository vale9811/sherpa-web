```label
Results
```

Results
=======

Most recent results (from Sherpa version 1.3.0) are available
[​here](http://projects.hepforge.org/sherpa/results/1.3.0/).

This page also contains results obtained with the Sherpa event generator
for:

-   [​NNLO
    distributions](http://www.slac.stanford.edu/~shoeche/pub/nnlo/7tev/)

Cross sections with Sherpa
--------------------------

-   [​e+e- colliders](http://comix.freacafe.de/ilc)
