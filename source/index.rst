.. _index:

Sherpa Homepage
==================================

Sherpa is a Monte Carlo event generator for the **S**\ imulation of
**H**\ igh-Energy **R**\ eactions of **PA**\ rticles in lepton-lepton,
lepton-photon, photon-photon, lepton-hadron and hadron-hadron
collisions. Simulation programs - also dubbed event generators - like
Sherpa are indispensable work horses for current particle physics
phenomenology and are (at) the interface between theory and
experiment.

- For a brief summary on the necessity and construction principles of
  event generators, see :ref:`monte-carlo-info`
- To browse the Sherpa manual online, see `the manual`_
- To download Sherpa, see :ref:`changelog`
- To find out more about the physics in Sherpa, see
  :ref:`publications` and :ref:`theses`.
- To look at results from and validation of Sherpa releases, see
  :ref:`Results`
- To get information about the authors of Sherpa, see
  :ref:`SherpaTeam`
- To ask questions and browse answers about Sherpa, see
  `the Sherpa Issue Tracker`_
- To be informed about patches and newer releases, subscribe to out
  `announcement mailing list`_

.. toctree::
   :hidden:
   :maxdepth: 2

   pages/test
   pages/team
   pages/monte-carlo
   pages/publications
   pages/theses
   pages/results
   changelog

.. _the manual: https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.7.html
.. _the Sherpa Issue Tracker: https://gitlab.com/sherpa-team/sherpa/issues/
.. _announcement mailing list: https://www.hepforge.org/lists/listinfo/sherpa-announce
